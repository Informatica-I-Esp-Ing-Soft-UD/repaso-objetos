from figura import Figura
from punto import Punto

class Triangulo (Figura):
    def __init__(self, p1, p2):
        Figura.__init__(self, p1, p2)
        self.nombre = 'Triangulo'
    
    def calcular_area(self):
        punto_temporal = Punto(self.punto1.x, self.punto2.y)
        lado1 = self.punto1.calcular_distancia(punto_temporal)
        lado2 = self.punto2.calcular_distancia(punto_temporal)
        self.area = (lado1 * lado2) / 2

    def calcular_perimetro(self):
        punto_temporal = Punto(self.punto1.x, self.punto2.y)
        hipo = self.punto1.calcular_distancia(self.punto2)
        lado1 = self.punto1.calcular_distancia(punto_temporal)
        lado2 = self.punto2.calcular_distancia(punto_temporal)
        self.perimetro = hipo + lado1 + lado2