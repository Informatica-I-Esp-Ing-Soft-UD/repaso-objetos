from productos import *

class Fabrica:
    def crear_arma(self):
        pass

    def crear_escudo(self):
        pass

class FabricaNuevaLinea:
    def crear_montura(self):
        pass

    def crear_yelmo(self):
        pass

class FabricaHumanos(Fabrica):
    def crear_arma(self):
        return ArmaHumanos()
    
    def crear_escudo(self):
        return EscudoHumanos()

# Adapter para incluir los 4 productos
class FabricaHumanosV2(Fabrica, FabricaNuevaLinea):
    def crear_arma(self):
        return ArmaHumanos()
    
    def crear_escudo(self):
        return EscudoHumanos()
    
    def crear_montura(self):
        return MonturaHumanos()

    def crear_yelmo(self):
        return YelmoHumanos()

class FabricaOrcos(Fabrica):
    def crear_arma(self):
        return ArmaOrcos()
    
    def crear_escudo(self):
        return EscudoOrcos()

# Adapter para incluir los 4 productos
class FabricaOrcosV2(Fabrica, FabricaNuevaLinea):
    def crear_arma(self):
        return ArmaOrcos()
    
    def crear_escudo(self):
        return EscudoOrcos()
    
    def crear_montura(self):
        return MonturaOrcos()

    def crear_yelmo(self):
        return YelmoOrcos()
    
    