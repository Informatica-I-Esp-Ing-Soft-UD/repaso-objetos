from flask import Flask, render_template, redirect, url_for
from fabricas import *
from random import choice

app = Flask(__name__)

@app.route('/')
def principal():
    return redirect(url_for('principal_v2'))

@app.route('/v1')
def principal_v1():
    fabricas = [FabricaHumanos(), FabricaOrcos()]
    fabrica = choice(fabricas)
    arma = fabrica.crear_arma()
    escudo = fabrica.crear_escudo()

    productos = []

    productos.append(arma)
    productos.append(escudo)

    return render_template("productos.html", productos = productos)

@app.route('/v2')
def principal_v2():
    fabricas = [FabricaHumanosV2(), FabricaOrcosV2()]
    fabrica = choice(fabricas)
    arma = fabrica.crear_arma()
    escudo = fabrica.crear_escudo()
    montura = fabrica.crear_montura()
    yelmo = fabrica.crear_yelmo()

    productos = []

    productos.append(arma)
    productos.append(escudo)
    productos.append(montura)
    productos.append(yelmo)

    return render_template("productos.html", productos = productos)

if __name__ == '__main__':
    app.run(debug=True)