class Producto:
    def __init__(self):
        self.imagen =  ""
        self.descripcion = ""
        self.grupo = ""

class Arma(Producto):
    def __init__(self):
        Producto.__init__(self)

class ArmaHumanos(Arma):
    def __init__(self):
        Arma.__init__(self)
        self.grupo = "Humanos"
        self.imagen = "imagenes/humanos/arma.png"
        self.descripcion = "Arma de los humanos"

class ArmaOrcos(Arma):
    def __init__(self):
        Arma.__init__(self)
        self.grupo = "Orcos"
        self.imagen = "imagenes/orcos/arma.png"
        self.descripcion = "Arma de los orcos"


class Escudo(Producto):
    def __init__(self):
        Producto.__init__(self)

class EscudoHumanos(Escudo):
    def __init__(self):
        Escudo.__init__(self)
        self.grupo = "Humanos"
        self.imagen = "imagenes/humanos/escudo.png"
        self.descripcion = "Escudo de los humanos"

class EscudoOrcos(Escudo):
    def __init__(self):
        Escudo.__init__(self)
        self.grupo = "Orcos"
        self.imagen = "imagenes/orcos/escudo.png"
        self.descripcion = "Escudo de los orcos"


class Montura(Producto):
    def __init__(self):
        Producto.__init__(self)
    
class MonturaHumanos(Montura):
    def __init__(self):
        Montura.__init__(self)
        self.grupo = "Humanos"
        self.imagen = "imagenes/humanos/montura.png"
        self.descripcion = "Montura de los humanos"

class MonturaOrcos(Montura):
    def __init__(self):
        Montura.__init__(self)
        self.grupo = "Orcos"
        self.imagen = "imagenes/orcos/montura.png"
        self.descripcion = "Montura de los orcos"


class Yelmo(Producto):
    def __init__(self):
        Producto.__init__(self)
    
class YelmoHumanos(Yelmo):
    def __init__(self):
        Yelmo.__init__(self)
        self.grupo = "Humanos"
        self.imagen = "imagenes/humanos/yelmo.png"
        self.descripcion = "Yelmo de los humanos"

class YelmoOrcos(Yelmo):
    def __init__(self):
        Yelmo.__init__(self)
        self.grupo = "Orcos"
        self.imagen = "imagenes/orcos/yelmo.png"
        self.descripcion = "Yelmo de los orcos"